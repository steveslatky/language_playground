-- Read more about this program in the official Elm guide:
-- https://guide.elm-lang.org/architecture/effects/time.html

import Html exposing (Html, button, div)
import Html.Events exposing (onClick)
import Svg exposing (..)
import Svg.Attributes exposing (..)
import Time exposing (Time, second)



main =
  Html.program
    { init = init
    , view = view
    , update = update
    , subscriptions = subscriptions
    }



-- MODEL


type alias Model = { time : Time
                    , paused : Bool }

init : (Model, Cmd Msg)
init =
  (Model 0 False, Cmd.none)



-- UPDATE


type Msg
  = Tick Time
  | Pause


update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
  case msg of
    Tick newTime ->
      ({model | time = newTime}, Cmd.none)

    Pause ->
      (toggle model  , Cmd.none)

-- Update Helpers
--
toggle : Model -> Model
toggle model =
    {model | paused = not(model.paused)}

-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    case model.paused of
      False ->
          Time.every second Tick
      True  ->
           Sub.none



-- VIEW


view : Model -> Html Msg
view model =
  let
    angleSec =
      turns (Time.inMinutes model.time)

    handXSec =
      toString (50 + 40 * cos angleSec)

    handYSec =
      toString (50 + 40 * sin angleSec)

    angleMin =
      turns (Time.inHours model.time)

    handXMin =
      toString (50 + 35 * cos angleMin)

    handYMin =
      toString (50 + 35 * sin angleMin)
  in
    div [] [ -- I didn't want to actually do the real math to get the accurate time at this moment.
    svg [ viewBox "0 0 100 100", width "300px" ]
      [ circle [ cx "50", cy "50", r "45", fill "#0B79CE" ] []
      , line [ x1 "50", y1 "50", x2 handXSec, y2 handYSec, stroke "#023963" ] []
      , line [ x1 "50", y1 "50", x2 handXMin, y2 handYMin, stroke "#023963" ] []
      ] ,
      div [] [button [onClick Pause] [text "Toggle clock"]]
      ]


