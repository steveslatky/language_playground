import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import String exposing (uncons, toInt )
import Char exposing (isUpper, isLower)


main =
  Html.beginnerProgram { model = model, view = view, update = update }


-- MODEL

type alias Model =
  { name : String
  , password : String
  , passwordAgain : String
  , age : String
  , validate : Bool
  }


model : Model
model =
  Model "" "" "" "" False


-- UPDATE

type Msg
    = Name String
    | Password String
    | PasswordAgain String
    | Age String
    | Submit


update : Msg -> Model -> Model
update msg model =
  case msg of
    Name name ->
      { model | name = name}

    Password password ->
      { model | password = password }

    PasswordAgain password ->
      { model | passwordAgain = password }

    Age age ->
      { model | age = age }

    Submit ->
      { model | validate = True }




-- VIEW

view : Model -> Html Msg
view model =
  div []
    [ input [ type_ "text", placeholder "Name", onInput Name ] []
    , input [ type_ "password", placeholder "Password", onInput Password ] []
    , input [ type_ "password", placeholder "Re-enter Password", onInput PasswordAgain ] []
    , input [ type_ "text", placeholder "Age", onInput Age] []
    , div []
     [ button [ onClick Submit ] [ text "Submit" ]]
    , viewValidation model
    , ageValidation model
    ]


hasLower : String -> Bool
hasLower password =
    case uncons password of
        Nothing -> False
        Just (h, t) ->   if isLower h  then True else hasLower t

hasUpper : String -> Bool
hasUpper password =
    case uncons password of
        Nothing -> False
        Just (h, t) ->   if isUpper h  then True else hasUpper t

viewValidation : Model -> Html msg
viewValidation model = -- Need to fix up how this is written. It is hard to read.
  let
    (color, message) =
        if model.validate then
            if model.password == model.passwordAgain then
                if String.length model.password > 8 then
                    if hasUpper model.password  &&  hasLower model.password then
                        ("green", "OK")
                    else ("red", "Password must contain a n upper and lowercase letter.")
                else ("red", "Password is not long enough!")
            else ("red", "Passwords do not match!")
        else ("White", "")
  in
    div [ style [("color", color)] ] [ text message ]

ageValidation : Model -> Html msg
ageValidation model =
     let
         (color, message) =
           if model.validate then
             if Result.withDefault 0 (toInt model.age) > 0 then
                ("","")
             else ("red", "Age entered is not a valid number")
           else ("White", "")
     in
        div [ style [("color", color)] ] [ text message ]